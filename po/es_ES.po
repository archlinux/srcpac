# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2004-2011
# This file is distributed under the same license as the srcpac package.
# 
# Translators:
# Christian Finnberg <christian@finnberg.net>, 2011.
#   <christian@finnberg.net>, 2011.
msgid ""
msgstr ""
"Project-Id-Version: srcpac\n"
"Report-Msgid-Bugs-To: https://bugs.archlinux.org/\n"
"POT-Creation-Date: 2011-10-12 01:45+0200\n"
"PO-Revision-Date: 2011-10-17 12:54+0000\n"
"Last-Translator: celf <christian@finnberg.net>\n"
"Language-Team: Spanish (Spain) (http://www.transifex.net/projects/p/srcpac/team/es_ES/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: es_ES\n"
"Plural-Forms: nplurals=2; plural=(n != 1)\n"

#: srcpac:36
msgid "usage: %s <operation> [...]"
msgstr "Uso: %s <operación> [...]"

#: srcpac:37
msgid "operations:"
msgstr "operaciones:"

#: srcpac:38
msgid "        %s {-h --help}"
msgstr "        %s {-h --help}"

#: srcpac:39
msgid "        %s {-V --version}"
msgstr "        %s {-V --version}"

#: srcpac:40
msgid "        %s {-Q --query}   [options] [<package(s)>]"
msgstr "        %s {-Q --query}   [opciones] [<paquete(s)>]"

#: srcpac:41
msgid "        %s {-R --remove}  [options] <package(s)>"
msgstr "        %s {-R --remove}  [opciones] <paquetes(s)>"

#: srcpac:42
msgid "        %s {-S --sync}    [options] [package(s)]"
msgstr "        %s {-S --sync}    [opciones] [paquete(s)]"

#: srcpac:43
msgid "%s options are based on pacman, so check the pacman man page"
msgstr ""
"Las opciones de %s están basadas en pacman, revisa la página man de pacman"

#: srcpac:44
msgid "%s adds some option to -S:"
msgstr "%s añade algunas opciones a -S:"

#: srcpac:45
msgid "  -b, --build    builds the targets from source"
msgstr "  -b, --build    compila todos los paquetes desde el origen"

#: srcpac:46
msgid "  -bb            builds targets and their dependencies from source"
msgstr ""
"  -bb            compila todos los paquetes y sus dependencias desde el "
"origen"

#: srcpac:47
msgid ""
"  -bbb           builds targets, dependencies and their makedeps from source"
msgstr ""
"  -bbb           compila todos los paquetes, sus dependencias y las "
"dependencias en tiempo de compilación desde el origen"

#: srcpac:48
msgid "  -m, --makedeps remove the makedepends after the build"
msgstr ""
"  -m, --makedeps borra las dependencias de compilación después de compilar"

#: srcpac:49
msgid ""
"  -o, --onlyconf applies the changes and displays the PKGBUILD without "
"building"
msgstr ""
"  -o, --onlyconf aplica los cambios y muestra el PKGBUILD sin compilar"

#: srcpac:50
msgid "%s adds some option to -Q:"
msgstr "%s añade algunas opciones a -Q:"

#: srcpac:51
msgid "  -b, --build    show all packages built from source"
msgstr ""
"  -b, --build    muestra todos los paquetes compilados desde el origen"

#: srcpac:52
msgid "%s adds some option to -R:"
msgstr "%s añade algunas opciones a -R:"

#: srcpac:53
msgid "  -b, --build    remove build-flag only, not the package"
msgstr ""
"  -b, --build    borra solo la marca de paquete compilado, no el paquete"

#: srcpac:54
msgid "  -o, --onlyconf remove the patchfile only"
msgstr "  -o, --onlyconf borra solo el patchfile"

#: srcpac:139
msgid "Error: Could not find %s under %s"
msgstr "Error: No se pudo encontrar %s en %s"

#: srcpac:148
msgid "Starting ABS sync..."
msgstr "Comenzando sincronización ABS..."

#: srcpac:150 srcpac:406
msgid "done"
msgstr "hecho"

#: srcpac:404
msgid "removing source reference %s..."
msgstr "Borrando la referencia al paquete %s"

#: srcpac:434
msgid "Error: You need to use sudo or to be root"
msgstr "Error: Necesitas usar sudo o ser root"

#: srcpac:439
msgid "Error: The ABSROOT environment variable is not defined"
msgstr "Error: La variable de entorno ABSROOT no está definida"

#: srcpac:481
msgid "Source         : "
msgstr "Fuente         : "

#: srcpac:483 srcpac:489
msgid "Yes"
msgstr "Si"

#: srcpac:485 srcpac:491
msgid "No"
msgstr "No"

#: srcpac:487
msgid "Patchfile      : "
msgstr "Patchfile      : "

#: srcpac:496
msgid " [source]"
msgstr " [fuente]"

#: srcpac:580
msgid "Source Targets:"
msgstr "Paquetes:"

#: srcpac:586
msgid "Proceed? [Y/n]"
msgstr "¿Proceder? [S/n]"

#: srcpac:588
msgid "y"
msgstr "s"

#: srcpac:588
msgid "Y"
msgstr "S"

#: srcpac:599
msgid "Build failed for:%s"
msgstr "Compilación fallida para:%s"


